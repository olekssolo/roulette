import java.util.Random;
public class RouletteWheel {
    Random rand = new Random();
    private int number = 0;

    public void spin() {
        number = rand.nextInt(37);
    }
    public int getValue() {
        return number;
    }
}